function clonobject (obj){
    let clon={} 
    for (k in obj){
        if (typeof obj[k] == "object"){
            clon[k]=clonobject(obj [k])
        }
        else{clon[k]=obj[k]}
    } 
    return clon
}
let obj1 ={a1:4, b:5, c:{d:5}}
let clon1=clonobject(obj1)
console.log (obj1)
console.log (clon1)
